import { getCoalitions, coalitionsWithQuotient, electedCandidatesByQuotient, sameCoalition, remainingElectedCandidates } from '../src/utils/elected';
import exampleDataFromTse from '../arquivos-de-exemplo/simulado/ele2022/9579/dados-simplificados/rn/rn-c0006-e009579-r.json'
import transformResults from '../src/utils/transformResults';
import { faker } from '@faker-js/faker';

const coalitions = [...Array(10)].map(() => faker.company.name());
const randomCoalition = () => coalitions[Math.round(Math.random() * (coalitions.length - 1))];

const createCandidate = (overrideData) => ({
  number: Math.random() * 10000,
  name: faker.name.fullName(),
  coalitionName: randomCoalition(),
  votes: Math.random() * 100_000,
  ...overrideData
});
const coalitionA = coalitions[0];
const coalitionB = coalitions[1];
const coalitionC = coalitions[2];
const coalitionD = coalitions[3];

// total votes: 360
const candidatesScenario1 = [
  createCandidate({ coalitionName: coalitionA, votes: 10 }),
  createCandidate({ coalitionName: coalitionA, votes: 50 }),
  createCandidate({ coalitionName: coalitionA, votes: 50 }),
  createCandidate({ coalitionName: coalitionA, votes: 40 }),
  createCandidate({ coalitionName: coalitionA, votes: 5 }),
  createCandidate({ coalitionName: coalitionB, votes: 10 }),
  createCandidate({ coalitionName: coalitionB, votes: 95 }),
  createCandidate({ coalitionName: coalitionB, votes: 4 }),
  createCandidate({ coalitionName: coalitionC, votes: 90 }),
  createCandidate({ coalitionName: coalitionC, votes: 4 }),
  createCandidate({ coalitionName: coalitionD, votes: 2 }),
];

//total votes 300
const candidatesScenario2 = [
  createCandidate({ coalitionName: coalitionA, votes: 150 }),
  createCandidate({ coalitionName: coalitionA, votes: 9 }),
  createCandidate({ coalitionName: coalitionB, votes: 41 }),
  createCandidate({ coalitionName: coalitionB, votes: 50 }),
  createCandidate({ coalitionName: coalitionB, votes: 45 }),
  createCandidate({ coalitionName: coalitionB, votes: 5 }),
];
//total votes 500
const candidatesScenario3 = [
  //360
  createCandidate({ coalitionName: coalitionA, votes: 350 }),
  createCandidate({ coalitionName: coalitionA, votes: 9 }),
  createCandidate({ coalitionName: coalitionA, votes: 1 }),
  createCandidate({ coalitionName: coalitionA, votes: 0 }),

  //103
  createCandidate({ coalitionName: coalitionB, votes: 41 }),
  createCandidate({ coalitionName: coalitionB, votes: 50 }),
  createCandidate({ coalitionName: coalitionB, votes: 7 }),
  createCandidate({ coalitionName: coalitionB, votes: 5 }),

  // 23
  createCandidate({ coalitionName: coalitionC, votes: 4 }),
  createCandidate({ coalitionName: coalitionC, votes: 4 }),
  createCandidate({ coalitionName: coalitionC, votes: 5 }),
  createCandidate({ coalitionName: coalitionC, votes: 5 }),
  createCandidate({ coalitionName: coalitionC, votes: 5 }),

  // 15
  createCandidate({ coalitionName: coalitionD, votes: 5 }),
  createCandidate({ coalitionName: coalitionD, votes: 5 }),
  createCandidate({ coalitionName: coalitionD, votes: 4 }),
];

const dataScenario4 = transformResults(exampleDataFromTse);

describe('Election calculations', () => {
  describe('#getCoalitions', () => {
    const candidates = [...Array(200)].map(createCandidate);
    it('returns a list of valid coalitions', () => {
      getCoalitions(candidates).forEach(coalition => expect(coalitions).toContain(coalition));
    });
    it('does not repeat coalitions', () => {
      const coalitions = getCoalitions(candidates);
      coalitions.forEach((coalition, i) => {
        const testArr = [...coalitions];
        testArr.splice(i, 1);
        expect(testArr).not.toContain(coalition);
      });
    });
    it('is coherent with data from tse', () => {
      const coalitions = getCoalitions(dataScenario4.candidates);
      expect(coalitions).toContain('P23');
      expect(coalitions).toContain('P16');
      expect(coalitions).toContain('P66 - FEDERAÇÃO ( P66 / P14 / P44 )');
    })
  });
  
  describe('#coalitionsWithQuotient', () => {
    it('returns an array of objects with name, quotient and total votes', () => {
      const candidates = [...Array(200)].map(createCandidate);
      coalitionsWithQuotient(candidates, 100).forEach(coalition => {
        expect(coalition).toHaveProperty('name');
        expect(coalition.totalVotes).toBeGreaterThan(0);
        expect(coalition.partyQuotient).toBeGreaterThanOrEqual(0);
      });
    });
    it('returns an array of object with valid coalition names', () => {
      const candidates = [...Array(200)].map(createCandidate);
      coalitionsWithQuotient(candidates, 100).map(({ name }) => name).forEach(coalition => expect(coalitions).toContain(coalition));
    });
    it('has correct calculations on party quotient', () => {
      // total votes: 360, total chairs = 5
      const result = coalitionsWithQuotient(candidatesScenario1, 72);
      // coalition A has 155 votes, 2.1 ~ 2 quotient
      expect(result.find(({ name }) => name === coalitionA).partyQuotient).toBe(2);
      // coalition B has 110 votes, 1.5 ~ 1 quotient
      expect(result.find(({ name }) => name === coalitionB).partyQuotient).toBe(1);
      // coalition C has 94 votes, 1.3 ~ 1 quotient
      expect(result.find(({ name }) => name === coalitionC).partyQuotient).toBe(1);
      // coalition D has 1 vote, 0.0... ~ 0 quotient
      expect(result.find(({ name }) => name === coalitionD).partyQuotient).toBe(0);
      
      // 6 chairs
      const result2 = coalitionsWithQuotient(candidatesScenario1, 60);
      // coalition A has 155 votes, 2.8 ~ 2 quotient
      expect(result2.find(({ name }) => name === coalitionA).partyQuotient).toBe(2);
      // coalition B has 110 votes, 1.8 ~ 1 quotient
      expect(result2.find(({ name }) => name === coalitionB).partyQuotient).toBe(1);
      // coalition C has 94 votes, 1.5 ~ 1 quotient
      expect(result2.find(({ name }) => name === coalitionC).partyQuotient).toBe(1);
      // coalition D has 1 vote, 0.0... ~ 0 quotient
      expect(result2.find(({ name }) => name === coalitionD).partyQuotient).toBe(0);
      // 8 chairs
      const result3 = coalitionsWithQuotient(candidatesScenario1, 45);
      // coalition A has 155 votes, 3.4 ~ 3 quotient
      expect(result3.find(({ name }) => name === coalitionA).partyQuotient).toBe(3);
      // coalition B has 110 votes, 2.4 ~ 2 quotient
      expect(result3.find(({ name }) => name === coalitionB).partyQuotient).toBe(2);
      // coalition C has 94 votes, 2.01 ~ 2 quotient
      expect(result3.find(({ name }) => name === coalitionC).partyQuotient).toBe(2);
      // coalition D has 1 vote, 0.0... ~ 0 quotient
      expect(result3.find(({ name }) => name === coalitionD).partyQuotient).toBe(0);
    });
  });
  describe('#electedCandidatesByQuotient', () => {
    // total votes: 360, total chairs = 5 
    const result = electedCandidatesByQuotient(candidatesScenario1, 72);
    it('returns the most popular candidates in each coalition', () => {
      const electedsCoalitionA = result.filter(sameCoalition(coalitionA));
      const electedsCoalitionB = result.filter(sameCoalition(coalitionB));
      const electedsCoalitionC = result.filter(sameCoalition(coalitionC));
      expect(electedsCoalitionA[0].votes).toBe(50);
      expect(electedsCoalitionA[1].votes).toBe(50);
      expect(electedsCoalitionB[0].votes).toBe(95);
      expect(electedsCoalitionC[0].votes).toBe(90);
    });
    it('returns the same number of candidates as the party quotient', () => {
      const electedsCoalitionA = result.filter(sameCoalition(coalitionA));
      const electedsCoalitionB = result.filter(sameCoalition(coalitionB));
      const electedsCoalitionC = result.filter(sameCoalition(coalitionC));
      const electedsCoalitionD = result.filter(sameCoalition(coalitionD));
      expect(electedsCoalitionA.length).toBe(2);
      expect(electedsCoalitionB.length).toBe(1);
      expect(electedsCoalitionC.length).toBe(1);
      expect(electedsCoalitionD.length).toBe(0);
      
    });
    it('does not return a candidate without engough electoral quotient', () => {
      // 3 chairs
      electedCandidatesByQuotient(candidatesScenario2, 100).forEach(({ votes }) => expect(votes).toBeGreaterThanOrEqual(10));
      // 5 chairs
      electedCandidatesByQuotient(candidatesScenario3, 100).forEach(({ votes }) => expect(votes).toBeGreaterThanOrEqual(10));
      
    });
  });
  describe('#remainingElectedCandidates', () => {
    const result1 = remainingElectedCandidates(candidatesScenario1, 72);
    // 6 chairs
    const result2 = remainingElectedCandidates(candidatesScenario1, 60);
    // 8 chairs
    const result3 = remainingElectedCandidates(candidatesScenario1, 45);
    
    // 3 chairs
    const result4 = remainingElectedCandidates(candidatesScenario2, 100);
    
    // 5 chairs
    const result5 = remainingElectedCandidates(candidatesScenario3, 100);
    
    it('returns the correct number of remeining chairs', () => {
      expect(result1.length).toBe(1);
      expect(result2.length).toBe(2);
      expect(result3.length).toBe(2);
      expect(result4.length).toBe(1);
      expect(result5.length).toBe(3);
    });
    it('distributes the remaining chairs correctly', () => {
      expect(result1[0].coalitionName).toBe(coalitionB);
      expect(result2[0].coalitionName).toBe(coalitionB);
      expect(result3[0].coalitionName).toBe(coalitionA);
      expect(result3[1].coalitionName).toBe(coalitionC);
      expect(result4[0].coalitionName).toBe(coalitionB);
      expect(result5[0].coalitionName).toBe(coalitionB);
      expect(result5[1].coalitionName).toBe(coalitionA);
      expect(result5[2].coalitionName).toBe(coalitionA);
    });
  });
});
