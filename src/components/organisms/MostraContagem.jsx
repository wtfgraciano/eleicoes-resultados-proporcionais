import ResultsTime from '../atoms/ResultsTime';
import GeneralVotesInfo from '../atoms/GeneralVotesInfo';
import CandidatesResults from '../molecules/CandidatesResults';
import { electedCandidates, notElectedCandidates } from '../../utils/elected';

export default function MostraContagem({
  time,
  countedVotes,
  validVotes,
  coalitionVotes,
  numberOfChairs,
  totalPossibleVotes,
  candidates,
}){

  const partialElectoralQuotient = numberOfChairs / totalPossibleVotes;
  const momentarilyElectedCandidates = electedCandidates(candidates, partialElectoralQuotient);
  const remainingCandidates = notElectedCandidates(candidates, partialElectoralQuotient);

  return (
    <section>
      <ResultsTime time={time} />
      <GeneralVotesInfo {...{countedVotes, validVotes, coalitionVotes, totalPossibleVotes}} />
      <CandidatesResults
        momentarilyElectedCandidates={momentarilyElectedCandidates}
        remainingCandidates={remainingCandidates}
      />
    </section>
  )
}