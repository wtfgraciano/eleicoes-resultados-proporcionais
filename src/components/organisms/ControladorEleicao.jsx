import { useEffect, useState } from 'preact/hooks';
import SeletorEstados from '../molecules/SeletorEstados';
import SeletorCargo from '../molecules/SeletorCargo';
import MostraContagem from './MostraContagem';
import Loading from '../atoms/Loading';
import Error from '../atoms/Error';
import useDataFromElection from '../../hooks/useDataFromElection';
import { COD_CARGOS } from '../../domain.json';
import transformResults from '../../utils/transformResults';

export default function SeletorEleicao() {
  const [codCargo, setCodCargo] = useState(COD_CARGOS.DEPUTADO_FEDERAL);
  const [uf, setUf] = useState('rn');
  const { loading, error, data } = useDataFromElection(uf, codCargo);

  return (
    <div>
      <SeletorCargo onChange={(value) => setCodCargo(value)} />
      <br />
      <SeletorEstados onChange={(value) => setUf(value)} />
      <br />
      {loading && <Loading />}
      {data && (<MostraContagem {...transformResults(data)} />)}
      {error && <Error error={error} />}
    </div>
  )
}