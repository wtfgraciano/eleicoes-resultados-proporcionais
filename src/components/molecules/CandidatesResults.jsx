import ListCandidates from './ListCandidates';

export default function MostraContagem({ momentarilyElectedCandidates, remainingCandidates }){
  return (
    <div>
      <h3>Candidatos <strong>momentaneamente</strong> eleitos</h3>
      <ListCandidates candidates={momentarilyElectedCandidates} />
      <h3>Candidatos restantes</h3>
      <ListCandidates candidates={remainingCandidates} />
    </div>
  )
}