import CandidateResult from '../atoms/CandidateResult';

export default function ListCandidates({ candidates }) {
  return (
    <ul>
        {candidates.map(candidate => <li key={`candidate-${candidate.number}`}><CandidateResult {...candidate} /></li>)}
      </ul>
  )
}