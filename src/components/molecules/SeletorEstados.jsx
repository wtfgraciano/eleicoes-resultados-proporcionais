import { useEffect, useState } from "preact/hooks"

export default function SeletorEstados({ onChange }) {

  const [uf, setUf] = useState();
  useEffect(() => uf && onChange(uf.toLowerCase()), [onChange, uf]);

  return (
    <select onChange={(ev) => setUf(ev.target.value)}>
      <option value="RN">RN</option>
      <option value="DF">DF</option>
    </select>
  )
}