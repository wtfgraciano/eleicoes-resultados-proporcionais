import { COD_CARGOS } from '../../domain.json';
import { useEffect, useState } from 'preact/hooks';


export default function SeletorCargo({ onChange }) {
  const [codCargo, setCodCargo] = useState();

  useEffect(() => codCargo && onChange(codCargo), [onChange, codCargo]);

  return (
    <select onChange={(ev) => setCodCargo(ev.target.value)}>
      <option value={COD_CARGOS.DEPUTADO_FEDERAL}>Deputados Federais</option>
      <option value={COD_CARGOS.DEPUTADO_DISTRITAL}>Deputados Distritais</option>
      <option value={COD_CARGOS.DEPUTADO_ESTADUAL}>Deputados Etaduais</option>
    </select>
  )
}