import { formatPercentage } from "../../utils/format";

export default function GeneralVotesInfo({
  countedVotes,
  validVotes,
  coalitionVotes,
  totalPossibleVotes,
}) {
  const nullVotes = countedVotes - validVotes;
    return (<p>Foram contados {countedVotes.toLocaleString('pt-BR')} de {totalPossibleVotes.toLocaleString('pt-BR')} votos ({formatPercentage(countedVotes, totalPossibleVotes)}), dos quais {validVotes} foram válidos ({formatPercentage(validVotes, countedVotes)}), {nullVotes.toLocaleString('pt-BR')} foram brancos e nulos ({formatPercentage(nullVotes, countedVotes)}) e {coalitionVotes.toLocaleString('pt-BR')} ({formatPercentage(coalitionVotes, validVotes)}) foram de legenda.</p>)
  }