export default function CandidateResult({
  number,
  name,
  elected,
  votes,
  coalitionName,
  hasElectoralQuotient
}){
  return (
    <section>
      <h4>{number} - {name} ({coalitionName})</h4>
      <p><strong>Total de votos: </strong> {votes.toLocaleString('pt-BR')}. <strong>{elected && 'ELEITO'}</strong></p>
      {hasElectoralQuotient && <p>Este candidato possui mínimo de 10% do quociente eleitoral neste momento</p>}
    </section>
  )
}