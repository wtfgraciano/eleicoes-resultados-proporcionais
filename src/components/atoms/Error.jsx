import { useEffect } from "preact/hooks";

export default function Error({ error }) {
  useEffect(() => console.log(error), [error])
  return (<span>Erro.</span>);
}