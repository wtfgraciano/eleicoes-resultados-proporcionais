import ControladorEleicao from '../organisms/ControladorEleicao';

export default function Main() {
  return <main>
    <h2>Resultados das Eleições Proporcionais 2022</h2>
    <ControladorEleicao />
  </main>
}