import { h } from 'preact';
import Main from './templates/Main';

const App = () => (
  <div id="app">
    <Main />
  </div>
)

export default App;
