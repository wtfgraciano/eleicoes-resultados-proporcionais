import { useEffect, useState } from 'preact/hooks';
import { CONFIG } from '../domain.json';
import zeroFill from '../utils/zeroFill';
import useFetch from './useFetch';

const {HOST, AMBIENTE, CICLO, ELEICAO} = CONFIG;

const DATA_PATH = `${HOST}/${AMBIENTE}/${CICLO}/${ELEICAO}/dados-simplificados`;

const urlSimplifiedData = (uf, codCargo) => `${DATA_PATH}/${uf}/${uf}-c${zeroFill(4, codCargo)}-e${zeroFill(6, ELEICAO)}-r.json`;

export default function useDataFromElection(uf, codCargo) {
  const [shouldRequest, setShouldRequest] = useState(uf && codCargo);
  const url = urlSimplifiedData(uf, codCargo);
  useEffect(() => setShouldRequest(uf && codCargo), [uf, codCargo]);
  return useFetch(url, shouldRequest);
}