import { useState, useEffect } from 'preact/hooks';

export default function useFetch(url, shouldRequest) {
  const [data, setData] = useState();
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    if(!shouldRequest) return;
    fetch(url).then((response) => response.json())
      .then((data) => setData(data))
      .catch((error) => setError(error))
      .finally(() => setLoading(false));
  }, [shouldRequest, url]);
  return {
    loading,
    data: !loading && !error && data,
    error: !loading && !data && error
  };
}