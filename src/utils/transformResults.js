export default function transformResults(dataFromTse) {
  return {
    time: dataFromTse.dt,
    endedTotalization: dataFromTse.tf,
    totalPossibleVotes: dataFromTse.e,
    countedVotes: dataFromTse.ea,
    validVotes: dataFromTse.vv,
    numberOfChairs: dataFromTse.v,
    coalitionVotes: dataFromTse.vl, // votos de legenda
    candidates: dataFromTse.cand.map((tseCandidate) => ({
      number: tseCandidate.n,
      name: tseCandidate.nm,
      coalitionName: tseCandidate.cc,
      elected: tseCandidate.e === 's',
      voteDestination: tseCandidate.dvt,
      votes: tseCandidate.vap,
    })).filter(({ voteDestination }) => !voteDestination.match(/anulado/i))
  }
}