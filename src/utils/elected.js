const byVotes = (a, b) => b.votes - a.votes; // DESC
const byMeans = (a, b) => b.mean - a.mean; // DESC
export const sameCoalition = (name) => ({coalitionName}) => name === coalitionName;
const enoughElectoralQuotient = electoralQuotient => ({ votes }) => votes >= (electoralQuotient / 10);
const candidateNotInList = list => ({ number }) => !list.find(c => c.number === number);
const totalValidVotes = candidates => candidates.reduce((total, { votes }) => total + votes, 0);

export const getCoalitions = candidates => [...new Set(candidates.map(({ coalitionName }) => coalitionName))];

export const coalitionsWithQuotient = (candidates, electoralQuotient) => getCoalitions(candidates)
.map((name) => ({
  name,
  totalVotes: candidates.filter(sameCoalition(name))
  .reduce((total, { votes }) => total + votes, 0),
}))
.map((coalition) => ({
  ...coalition,
  partyQuotient: Math.floor(coalition.totalVotes / electoralQuotient),
}));

export const electedCandidatesByQuotient = (candidates, electoralQuotient) => coalitionsWithQuotient(candidates, electoralQuotient)
.map(({ name, partyQuotient }) => candidates
.filter(sameCoalition(name))
.filter(enoughElectoralQuotient(electoralQuotient))
.sort(byVotes)
.slice(0, partyQuotient)
).flat()
.map(candidate => ({
  ...candidate,
  hasElectoralQuotient: true,
}));

export const remainingElectedCandidates = (candidates, electoralQuotient) => {
  const electedByQuotient = electedCandidatesByQuotient(candidates, electoralQuotient);
  const coalitions = coalitionsWithQuotient(candidates, electoralQuotient).map(coalition => ({
    ...coalition,
    mean: 0,
    filledChairs: electedByQuotient.filter(sameCoalition(coalition.name)).length,
  }));
  const totalFilledChairs = () => coalitions.reduce((total, { filledChairs }) => total + filledChairs, 0);
  const totalChairs = totalValidVotes(candidates) / electoralQuotient;
  const calculateMeans = () => coalitions.forEach(coalition => {
    coalition.mean = coalition.totalVotes / (coalition.filledChairs+1);
  });
  const resultingCandidates = [];
  
  const remainingCandidates = () => candidates
    .filter(candidateNotInList(electedByQuotient))
    .filter(candidateNotInList(resultingCandidates));
  calculateMeans();
  for(let i=0; i<coalitions.sort(byMeans).length; i++) {
    const coalition = coalitions.sort(byMeans)[i];
    const candidate = remainingCandidates().filter(sameCoalition(coalition.name)).sort(byVotes)[0];
    if(candidate && enoughElectoralQuotient(electoralQuotient)(candidate)) {
      resultingCandidates.push(candidate);
      coalition.filledChairs++;
      break;
    }
  }

  let stackSize = 50;

  while(totalFilledChairs() < totalChairs && stackSize >=0) {
    stackSize--;
    calculateMeans();

    const mostMeansCoalition = coalitions.sort(byMeans)[0];
    const candidate = remainingCandidates().filter(sameCoalition(mostMeansCoalition.name)).sort(byVotes)[0];
    if (candidate) {
      resultingCandidates.push(candidate);
      mostMeansCoalition.filledChairs++;
    }

  }
  return resultingCandidates;
};

export const electedCandidates = (candidates, electoralQuotient) => [...electedCandidatesByQuotient(candidates, electoralQuotient), ...remainingElectedCandidates(candidates, electoralQuotient)];

export const notElectedCandidates = (candidates, electoralQuotient) => candidates
.filter(candidateNotInList(electedCandidates(candidates, electoralQuotient)))
.sort(byVotes);