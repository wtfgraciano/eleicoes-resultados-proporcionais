export function formatPercentage(fraction, total) {
  const num = Math.round((fraction / total) * 1000) / 10;
  return `${num.toLocaleString('pt-BR')} %`;
}